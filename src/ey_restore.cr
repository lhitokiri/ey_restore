require "halite"
require "myhtml"
require "modest"
require "http/client"

URL_LOGIN = "https://login.engineyard.com/login"

request1 = Halite.get(URL_LOGIN)

myhtml = Myhtml::Parser.new(request1.body)

csrf_token = myhtml.css("meta[name=csrf-token]")
                   .to_a[0]
                   .attribute_by("content")

form_data = { "email" => ENV["EMAIL"], "password" => ENV["PASSWORD"], "authenticity_token" => csrf_token }

request2 = Halite.cookies(_sso_anon_session: request1.cookies["_sso_anon_session"].value)
                 .post(URL_LOGIN, form: form_data)

request3 = Halite.cookies(_sso_anon_session: request2.cookies["_sso_anon_session"].value,
                          _sso_auth: request2.cookies["_sso_auth"].value)
                 .get(request2.headers["Location"])

request4 = Halite.cookies(eylocale: request3.cookies["eylocale"].value,
                          _cloud_engineyard: request3.cookies["_cloud_engineyard"].value)
                 .get(request3.headers["Location"])

request5 = Halite.cookies(_sso_anon_session: request2.cookies["_sso_anon_session"].value,
                          _sso_auth: request2.cookies["_sso_auth"].value)
                 .get(request4.headers["Location"])

request6 = Halite.cookies(_sso_anon_session: request5.cookies["_sso_anon_session"].value,
                          _sso_auth: request2.cookies["_sso_auth"].value)
                 .get(request5.headers["Location"])

request7 = Halite.cookies(_cloud_engineyard: request4.cookies["_cloud_engineyard"].value)
                 .get(request6.headers["Location"])

request8 = Halite.cookies(_cloud_engineyard: request7.cookies["_cloud_engineyard"].value)
                 .get(request7.headers["Location"])

env_path = Myhtml::Parser.new(request8.body)
                         .css("#environment117493 > div:nth-child(1) > h2:nth-child(2) > span:nth-child(1) > a:nth-child(1)")
                         .to_a[0]
                         .attribute_by("href")

request9 = Halite.cookies(_cloud_engineyard: request8.cookies["_cloud_engineyard"].value)
                 .get("https://cloud.engineyard.com#{env_path}".gsub("environment", "backups"))

backup_link = Myhtml::Parser.new(request9.body)
                            .css("ul.backups > li:nth-child(1) > a:nth-child(1)")
                            .to_a[0]
                            .attribute_by("href")
                            .as(String)

HTTP::Client.get(backup_link) do |response|
  File.write("backup", response.body_io)
end
